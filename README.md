# LiveDocs Tutorial Hofgeismar

### CRC 1456 Binderhub Jupyterlab
http://c109-005.cloud.gwdg.de:30901/v2/gwdg/pedrocklein%2Flivedocs-tutorial-hofgeismar/HEAD

### CRC 1456 Binderhub Jupyter Classic (Retro)
http://c109-005.cloud.gwdg.de:30901/v2/gwdg/pedrocklein%2Flivedocs-tutorial-hofgeismar/HEAD?urlpath=tree

### CRC 1456 Binderhub Jupyterlab Voila
http://c109-005.cloud.gwdg.de:30901/v2/gwdg/pedrocklein%2Flivedocs-tutorial-hofgeismar/HEAD?urlpath=voila

### CRC 1456 Jupyterlite
https://pedrocklein.pages.gwdg.de/livedocs-tutorial-hofgeismar

### Static HTML "sample_notebook.html"
https://livedocs-tutorial-hofgeismar-pedrocklein-cb0b0c5a1a8e0ffd51fc8e.pages.gwdg.de/files/sample_jupyter_notebook.html
